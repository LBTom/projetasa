package fr.univnantes.asa.server;
public class Check_query {
	
   	private SecurityManagement secu;

	public Check_query(SecurityManagement secu) {
		super();
		this.secu = secu;
	}

	public SecurityManagement getSecu() {
		return secu;
	}

	public void setSecu(SecurityManagement secu) {
		this.secu = secu;
	}

}	
