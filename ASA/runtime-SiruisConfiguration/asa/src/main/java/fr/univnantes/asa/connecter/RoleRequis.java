package fr.univnantes.asa.connecter;

import fr.univnantes.asa.server.PortFournis;

public class RoleRequis{

	private PortFournis port;

	public RoleRequis(PortFournis port) {
		super();
		this.port = port;
	}

	public PortFournis getPort() {
		return port;
	}

	public void setPort(PortFournis port) {
		this.port = port;
	}
	

}
