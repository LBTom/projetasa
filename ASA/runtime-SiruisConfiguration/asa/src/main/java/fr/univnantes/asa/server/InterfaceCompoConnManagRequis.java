package fr.univnantes.asa.server;
import java.security.Provider.Service;

public class InterfaceCompoConnManagRequis{
	
   	private boolean requis;
   	private String nom;
   	private SecurityCheck SecurityCheck;
   	private DB_query DB_query;

	public void InterfaceCompoConnManagRequis(){}

	public boolean isRequis() {
		return requis;
	}

	public InterfaceCompoConnManagRequis(boolean requis, String nom, SecurityCheck securityCheck, DB_query dB_query) {
		super();
		this.requis = requis;
		this.nom = nom;
		SecurityCheck = securityCheck;
		DB_query = dB_query;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public SecurityCheck getSecurityCheck() {
		return SecurityCheck;
	}

	public void setSecurityCheck(SecurityCheck securityCheck) {
		SecurityCheck = securityCheck;
	}

	public DB_query getDB_query() {
		return DB_query;
	}

	public void setDB_query(DB_query dB_query) {
		DB_query = dB_query;
	}




}	
