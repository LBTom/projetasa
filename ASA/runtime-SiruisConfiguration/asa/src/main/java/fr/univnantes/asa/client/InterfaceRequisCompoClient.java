package fr.univnantes.asa.client;

public class InterfaceRequisCompoClient{
	
   	private boolean requis;
   	private String nom;
   	private PortClientRequis PortClientRequis;

	public InterfaceRequisCompoClient(){
		this.PortClientRequis = new PortClientRequis();
	}

	public InterfaceRequisCompoClient(boolean requis, String nom,
			fr.univnantes.asa.client.PortClientRequis portClientRequis) {
		super();
		this.requis = requis;
		this.nom = nom;
		PortClientRequis = portClientRequis;
	}

	public boolean isRequis() {
		return requis;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public PortClientRequis getPortClientRequis() {
		return PortClientRequis;
	}

	public void setPortClientRequis(PortClientRequis portClientRequis) {
		PortClientRequis = portClientRequis;
	}



}	
