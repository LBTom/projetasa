package fr.univnantes.asa.connecter;

public class RPCFournis {

	private RoleFournis RoleFournis;

	public RPCFournis(fr.univnantes.asa.connecter.RoleFournis roleFournis) {
		super();
		RoleFournis = roleFournis;
	}

	public RPCFournis() {
	}

	public RoleFournis getRoleFournis() {
		return RoleFournis;
	}

	public void setRoleFournis(RoleFournis roleFournis) {
		RoleFournis = roleFournis;
	}

}
