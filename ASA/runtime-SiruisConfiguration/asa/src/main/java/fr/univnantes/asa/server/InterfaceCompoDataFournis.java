package fr.univnantes.asa.server;
import java.security.Provider.Service;

public class InterfaceCompoDataFournis{
	
   	private boolean requis;
   	private String nom;
   	private Query_Interrogation Query_Interrogation;
   	private SecurityManagement SecurityManagement;

	public InterfaceCompoDataFournis(boolean requis, String nom, Query_Interrogation query_Interrogation,
			SecurityManagement securityManagement) {
		super();
		this.requis = requis;
		this.nom = nom;
		Query_Interrogation = query_Interrogation;
		SecurityManagement = securityManagement;
	}

	public void InterfaceCompoDataFournis(){}

	public boolean isRequis() {
		return requis;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Query_Interrogation getQuery_Interrogation() {
		return Query_Interrogation;
	}

	public void setQuery_Interrogation(Query_Interrogation query_Interrogation) {
		Query_Interrogation = query_Interrogation;
	}

	public SecurityManagement getSecurityManagement() {
		return SecurityManagement;
	}

	public void setSecurityManagement(SecurityManagement securityManagement) {
		SecurityManagement = securityManagement;
	}

}	
