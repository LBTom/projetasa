package fr.univnantes.asa.server;
import java.security.Provider.Service;

public class InterfaceCompoSecuManagFournis{
	
   	private boolean requis;
   	private String nom;
   	private Security_Autentification Security_Autentification;

	public void InterfaceCompoSecuManagFournis(){}

	public boolean isRequis() {
		return requis;
	}

	public InterfaceCompoSecuManagFournis(boolean requis, String nom, Security_Autentification security_Autentification) {
		super();
		this.requis = requis;
		this.nom = nom;
		Security_Autentification = security_Autentification;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Security_Autentification getSecurity_Autentification() {
		return Security_Autentification;
	}

	public void setSecurity_Autentification(Security_Autentification security_Autentification) {
		Security_Autentification = security_Autentification;
	}




}	
