package fr.univnantes.asa;

import fr.univnantes.asa.client.Client;
import fr.univnantes.asa.connecter.RPC;
import fr.univnantes.asa.connecter.RoleFournis;
import fr.univnantes.asa.server.Serveur;

public class SystemComplet {

    private Client Client;
    private Serveur Serveur;
    private	RPC RPC;
	

	public SystemComplet(fr.univnantes.asa.client.Client client, fr.univnantes.asa.server.Serveur serveur,
			fr.univnantes.asa.connecter.RPC rPC) {
		super();
		Client = client;
		Serveur = serveur;
		RPC = rPC;
	}

	public Client getClient() {
		return Client;
	}

	public void setClient(Client client) {
		Client = client;
	}

	public Serveur getServeur() {
		return Serveur;
	}

	public void setServeur(Serveur serveur) {
		Serveur = serveur;
	}

	public RPC getRPC() {
		return RPC;
	}

	public void setRPC(RPC rPC) {
		RPC = rPC;
	}


}
