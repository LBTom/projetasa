package fr.univnantes.asa.server;
import java.security.Provider.Service;

public class InterfaceCompoSecuManagRequis{
	
   	private boolean requis;
   	private String nom;
   	private Check_query Check_query;

	public void InterfaceCompoSecuManagRequis(){}

	public boolean isRequis() {
		return requis;
	}

	public InterfaceCompoSecuManagRequis(boolean requis, String nom, Check_query check_query) {
		super();
		this.requis = requis;
		this.nom = nom;
		Check_query = check_query;
	}

	public void setRequis(boolean requis) {
		this.requis = requis;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Check_query getCheck_query() {
		return Check_query;
	}

	public void setCheck_query(Check_query check_query) {
		Check_query = check_query;
	}



}	
