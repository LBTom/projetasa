public class Service {

    java.lang.String nom;

    Service service;
    Service serviceeOpposite;

    public Service(){};

    public java.lang.String getNom () {
        return this.nom;
    }

    public void setNom (java.lang.String nom) {
        this.nom = nom;
    }

    public Service getService () {
        return this.service;
    }
    public Service getServiceeOpposite () {
        return this.serviceeOpposite;
    }

    public void setService (Service service) {
        this.service = service;
    }
    public void setServiceeOpposite (Service serviceeOpposite) {
        this.serviceeOpposite = serviceeOpposite;
    }


}
